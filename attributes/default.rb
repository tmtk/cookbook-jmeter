["2.8", "2.9"].each do |ver|
  default[:jmeter][ver][:home]    = "#{ENV['HOME']}/.jmeter"
  default[:jmeter][ver][:release] = "http://ftp.riken.jp/net/apache//jmeter/binaries/apache-jmeter-#{ver}.tgz"
end

default[:jmeter][:version] = "2.9"
version = default[:jmeter][:version]
default[:jmeter][:home]    = default[:jmeter][version][:home]
default[:jmeter][:release] = default[:jmeter][version][:release]
default[:jmeter][:archive] = "/tmp/" + (File.basename default.jmeter.release)
