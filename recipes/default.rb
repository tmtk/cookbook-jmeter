remote_file node.jmeter.archive do
  source node.jmeter.release
  not_if {::File.exists? node.jmeter.archive}
end

execute "tar xfzp #{node.jmeter.archive}" do
  cwd File.dirname node.jmeter.archive
end

file = node.jmeter.archive
path = File.dirname(file) + "/" + File.basename(file, File.extname(file))

execute "mv #{path} #{node.jmeter.home}"
